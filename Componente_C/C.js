const SERVER_PORT = 8001;
const http = require('http'); // Import Node.js core module

const componentC = http.createServer(function (req, res) {   //create web componentC
    if (req.url === '/') {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write('<html lang="en"><body><h1>Hello! I am the component C.</h1></body></html>');
        res.end();

    } else if (req.url === "/data") {
        const fs = require('fs');
        const fileContent = JSON.parse(fs.readFileSync('data.json', 'utf8'));

        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(fileContent, null, 3));

    } else
        res.end('Invalid Request!');

});

//Listen for any incoming requests
componentC.listen(SERVER_PORT);

console.info(`Component C is running on port ${SERVER_PORT}!`);
