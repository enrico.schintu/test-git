import ConditionalField from '@/models/conditional-field.model';
import DocumentDto from '@/models/document.model';

/**
 * This Class will parse a document that use the GLML syntax.
 */
export default class DocumentParser {

    public static parseDocument(document: DocumentDto): string {
        const {fields} = document;
        let output = JSON.parse(JSON.stringify(document.body || ''));
        output = this.resolveConditionalFields(output, fields);
        output = this.resolveFieldPlaceHolder(output, fields);
        return output;
    }

    public static getAllFields(documentBody: string): string[] {
        const FIELD_REGEX = new RegExp('\\${(.*?)\\}', 'gm');
        const result: string[] = [];
        const placeholders = documentBody.match(FIELD_REGEX) || [];
        placeholders.map((p) => p.toString())
            .map((p) => p.replace('${', '')
                .replace('}', ''))
            .filter((p) => !result.includes(p) && !p.includes('${'))
            .forEach((p) => result.push(p));
        return result;
    }

    private static resolveFieldPlaceHolder(body: string = '', fields: any) {
        const placeholders = Object.keys(fields).reduce((acc, cur) => {
            acc.set('${' + cur + '}', fields[cur]);
            return acc;
        }, new Map<string, string>());
        Array.from(placeholders.keys())
            .forEach((key) => body = body.split(key).join(` ${placeholders.get(key)}`));
        placeholders.forEach((value, key) => body.replace(key, value));
        return body;
    }

    private static resolveConditionalFields(body: string = '', fields: any): string {
        const conditionalsFields: ConditionalField[] = this.getAllConditionalFields(body);
        conditionalsFields.forEach((cond) => body = body.replace(cond.rawString, (fields[cond.field] === cond.equalsTo) ? cond.value : ''));
        return body;
    }

    private static getAllConditionalFields(document: string): ConditionalField[] {
        const CONDITIONAL_FIELD_REGEX = new RegExp('\\$\\${If(.*?)(.*?)Insert}{(.*?)}', 'g');
        const placeholders = document.match(CONDITIONAL_FIELD_REGEX) || [];
        return placeholders.map((p) => p.toString())
            .map((cf) => this.createConditionalField(cf));
    }

    // tslint:disable
    private static createConditionalField(rawString: string): ConditionalField {
        const TARGET_FIELD = new RegExp('^(.*?) =', 'g');
        const EQUALS_VALUE = new RegExp('\\[(.*?)\\]', 'g');
        const FIELD_VALUE = new RegExp('\\{(.*?)\\}', 'g');

        const normalizedTarget = rawString
            .replace('$${If ${', '')
            .replace('}', '')
            .replace('${ ', '')
            .replace('; Insert}', '');

        try {
            // @ts-ignore
            const field = normalizedTarget.match(TARGET_FIELD)[0].replace(' =', '');
            // @ts-ignore
            const equalsTo = normalizedTarget.match(EQUALS_VALUE)[0].replace('[', '').replace(']', '');
            // @ts-ignore
            const value = normalizedTarget.match(FIELD_VALUE)[0].replace('{', '').replace('}', '');

            return {rawString, field, equalsTo, value};
        } catch (ex) {
            console.warn(ex);
            return {rawString: '', field: '', equalsTo: '', value: ''};
        }
    }


}
