import {JWT_TOKEN_KEY, USERNAME_TOKEN_KEY} from '@/consts/app.constant';

export class AuthUtils {

    public static isAuthenticated(): boolean {
        return !!localStorage.getItem(JWT_TOKEN_KEY);
    }

    public static setAuthentication(token: string, username: string): void {
        localStorage.setItem(JWT_TOKEN_KEY, token);
        localStorage.setItem(USERNAME_TOKEN_KEY, username);
    }

    public static removeAuthentication(): void {
        localStorage.removeItem(JWT_TOKEN_KEY);
        localStorage.removeItem(USERNAME_TOKEN_KEY);
    }

    public static authorizedHeader(): any {
        return this.isAuthenticated() ? {headers: {X_AUTH_TOKEN: localStorage.getItem(JWT_TOKEN_KEY)}} : null;
    }

    public static username(): string {
        return this.isAuthenticated() ? localStorage.getItem(USERNAME_TOKEN_KEY) || '' : '';
    }
}
