import Vue from 'vue';
import Login from './views/Login.vue';
import {AuthUtils} from '@/utils/auth-utils';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login,
        },
        {
            path: '/home',
            name: 'home',
            component: () => import(/* webpackChunkName: "home" */ './views/Home.vue'),
            beforeEnter: (to, from, next) => {
                return AuthUtils.isAuthenticated() ? next() : next('/');
            },
        },
        {
            path: '/change-password',
            name: 'changePassword',
            component: () => import('./views/ChangePassword.vue'),
            beforeEnter: (to, from, next) => {
                return from === to ? next(false) : AuthUtils.isAuthenticated() ? next() : next('/');
            },
        },
    ],
});
