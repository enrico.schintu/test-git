export default interface ConditionalField {
    rawString: string;
    field: string;
    equalsTo: string;
    value: string;
}
