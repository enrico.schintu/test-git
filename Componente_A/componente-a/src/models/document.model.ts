export default interface DocumentDto {
    id?: number;
    title?: string;
    body?: string;
    fields?: any;
    updatedBy?: string;
    updatedAt?: Date;

    [key: string]: any | any[];
}
