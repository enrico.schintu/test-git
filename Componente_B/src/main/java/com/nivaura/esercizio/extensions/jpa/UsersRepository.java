package com.nivaura.esercizio.extensions.jpa;

import com.nivaura.esercizio.models.dao.UserDao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<UserDao, Long> {

    UserDao findByUsername(String username);

}
