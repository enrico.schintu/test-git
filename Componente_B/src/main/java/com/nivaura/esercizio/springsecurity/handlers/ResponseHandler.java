package com.nivaura.esercizio.springsecurity.handlers;

import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * This contract reach all possible handler used by Spring security to manage the
 * authentication/access request and manages the responses for the client-side. An implementation of
 * this contract must be implemented the method to manage the response for example to redirect the
 * page URLs to a specific page or to simply return a HTTP response codes useful to delegate the
 * behavior to the client-side.
 *
 * @author Enrico Schintu
 */
public interface ResponseHandler
    extends AuthenticationSuccessHandler,
        LogoutSuccessHandler,
        AuthenticationEntryPoint,
        AccessDeniedHandler,
        AuthenticationFailureHandler {}
