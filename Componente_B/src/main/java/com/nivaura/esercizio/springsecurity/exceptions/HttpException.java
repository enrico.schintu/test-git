package com.nivaura.esercizio.springsecurity.exceptions;

import org.apache.http.HttpStatus;

/**
 * Abstract exception generally used during http operation or processes, for example during an HTTP
 * request to the server. It provides an {@link HttpStatus} code to catalog the HTTP error (it is
 * also available its integer value code) and the exception cause.
 *
 * @author Enrico Schintu
 */
public abstract class HttpException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private String message;

  private Integer httpStatus;

  /**
   * Initialize a {@link HttpException} with the message to display and related HTTP code.
   *
   * @param message
   * @param httpStatus
   */
  public HttpException(String message, Integer httpStatus) {
    super();
    this.message = message;
    this.httpStatus = httpStatus;
  }

  /**
   * The wrapper method of the {@link #message} property.
   *
   * @return the value of the property
   */
  @Override
  public String getMessage() {
    return message;
  }

  /**
   * The wrapper method of the {@link #httpStatus} property.
   *
   * @return the value of the property
   */
  public Integer getHttpStatus() {
    return httpStatus;
  }

  /**
   * The wrapper method of the {@link #httpStatus} value property.
   *
   * @return the integer value of the HttpStatus property
   */
  public int getHttpStatusCode() {
    return httpStatus;
  }
}
