package com.nivaura.esercizio.springsecurity.exceptions;

import org.apache.http.HttpStatus;

/**
 * This exception will be thrown when authentication is required and has failed or has not yet been
 * provided or for example if it tempts to access on a resource without a pre-login phase.
 *
 * <p>By default its HTTP error code is <b>401</b> (Unauthorized). It is possible configure the
 * message exception during initialization.
 *
 * @author Enrico Schintu
 */
public class UnauthorizedException extends HttpException {

  private static final long serialVersionUID = 6287156180399536764L;

  /** @param message */
  public UnauthorizedException(String message) {
    super(message, HttpStatus.SC_UNAUTHORIZED);
  }
}
