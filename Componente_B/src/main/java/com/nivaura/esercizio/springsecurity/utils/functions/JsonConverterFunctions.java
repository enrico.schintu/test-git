package com.nivaura.esercizio.springsecurity.utils.functions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * A container of simple routines for JSON convert operations.
 *
 * @author Enrico Schintu
 */
public abstract class JsonConverterFunctions {

  /**
   * Convert a passed object into a JSON format. If the object passed is a DAO, the JSON generated
   * will build with as keys the name of the properties and as value their values. It is also
   * possible convert the array properties.
   *
   * <p>By default the return JSON string is not pretty printed.
   *
   * @param inputObject to convert
   * @return the JSON result in String type
   * @throws JsonProcessingException
   */
  public static String convertObjectToJson(Object inputObject) throws JsonProcessingException {
    return convertObjectToJson(inputObject, false);
  }

  /**
   * Convert a passed object into a JSON format. If the object passed is a DAO, the JSON generated
   * will build with as keys the name of the properties and as value their values. It is also
   * possible convert the array properties.
   *
   * <p>It is possible decide if the return JSON string will be pretty printed.
   *
   * @param inputObject
   * @param prettyPrint boolean value
   * @return
   * @throws JsonProcessingException
   */
  public static String convertObjectToJson(Object inputObject, boolean prettyPrint)
      throws JsonProcessingException {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    objectMapper.registerModule(new SimpleModule());
    String objectFormatted = "";
    if (prettyPrint) {
      objectFormatted =
          objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(inputObject);
    } else {
      objectFormatted = objectMapper.writeValueAsString(inputObject);
    }
    return objectFormatted;
  }
}
