package com.nivaura.esercizio.springsecurity.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nivaura.esercizio.springsecurity.exceptions.InvalidTokenException;
import com.nivaura.esercizio.springsecurity.utils.functions.JsonConverterFunctions;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class exposes useful methods and constants for managing tokens.
 *
 * @author Enrico Schintu
 */
public class JwtUtils {

    public static final String TOKEN_PREFIX = "Bearer ";

    public static final String TOKEN_HEADER_KEY = "X_AUTH_TOKEN";

    private static final long EXPIRATION_TIME = 86_400_000; // 1 day

    private static final String AUTHORITIES_KEY = "auth";

    private static final String SECRET_KEY = "45E3FW3aWY7bdyWskkkaRee6yyHu3vcH";

    /**
     * Return the JWT token generated with the informations of {@link Authentication} provided as
     * input
     *
     * @param authentication
     * @return
     */
    public static String createToken(Authentication authentication) {
        String authorities =
                authentication.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(Collectors.joining(","));
        return TOKEN_PREFIX
                + Jwts.builder()
                .setSubject(authentication.getName())
                .claim(AUTHORITIES_KEY, authorities)
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY.getBytes())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .compact();
    }

    /**
     * Returns the subject contained in the token provided as input
     *
     * @param bearerToken
     * @return
     * @throws InvalidTokenException if an error occurs during the manipulation of token, to
     *                               intercept:
     *                               <ul>
     *                                 <li>UnsupportedJwtException
     *                                 <li>JWSMalformedJwtException
     *                                 <li>JWSSignatureException
     *                                 <li>ExpiredJwtException
     *                                 <li>IllegalArgumentException
     *                               </ul>
     *                               during the parse claim of JWT Token
     */
    public static String resolveToken(String bearerToken) throws InvalidTokenException {
        String result;
        try {
            result =
                    Jwts.parser()
                            .setSigningKey(SECRET_KEY.getBytes())
                            .parseClaimsJws(bearerToken.replace(TOKEN_PREFIX, ""))
                            .getBody()
                            .getSubject();
        } catch (Exception e) {
            throw new InvalidTokenException(
                    "Error encountered while reading the token: " + TOKEN_HEADER_KEY, e);
        }
        return result;
    }
}
