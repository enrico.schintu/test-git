package com.nivaura.esercizio.springsecurity.exceptions;

import org.apache.http.HttpStatus;

/**
 * This exception will be thrown during an error occurs during an internal process on server side
 * operation.
 *
 * <p>By default its HTTP error code is <b>500</b> (Internal server error). It is possible configure
 * the message exception during initialization.
 *
 * @author Enrico Schintu
 */
public class InternalErrorException extends HttpException {

  private static final long serialVersionUID = 129562188745724753L;

  /** @param message */
  public InternalErrorException(String message) {
    super(message, HttpStatus.SC_INTERNAL_SERVER_ERROR);
  }
}
