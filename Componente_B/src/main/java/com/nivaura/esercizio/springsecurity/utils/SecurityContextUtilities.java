
package com.nivaura.esercizio.springsecurity.utils;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;

/**
 * This utilities class provide some wrapping methods for the Security context. The spring security
 * framework stores the user logged information into the {@link SecurityContextHolder}, this
 * utilities class allows to access to these information centralized the calls around the code.
 *
 * @author Enrico Schintu
 */
public abstract class SecurityContextUtilities {


    /**
     * Method to retrieve the current username into the session. If no user has been logged it will
     * return null.
     *
     * @return username if there is an user logged, else <b>null</b>
     */
    public static String getUsername() {
        String username = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && !(authentication instanceof AnonymousAuthenticationToken)) {
            username = authentication.getName();
        }
        return username;
    }

    /**
     * Method to retrieve the granted authorities of the user logged. If no user has been logged, it
     * will return null.
     *
     * @return the collection authorities if an user is logged, else <b>null</b>
     */
    public static Collection<? extends GrantedAuthority> getGrantedAuthorities() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && !(authentication instanceof AnonymousAuthenticationToken)) {
            return SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        }
        return null;
    }
}
