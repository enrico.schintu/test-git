package com.nivaura.esercizio.springsecurity.handlers;

import com.nivaura.esercizio.springsecurity.exceptions.ForbiddenAccessException;
import com.nivaura.esercizio.springsecurity.exceptions.UnauthorizedException;
import com.nivaura.esercizio.springsecurity.utils.JwtUtils;
import com.nivaura.esercizio.springsecurity.utils.WebUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Simple http interceptor response handler to return specifics codes to the client side. It
 * returns:
 *
 * <ul>
 *   <li><b>401 HTTP code</b> in front of an error during authentication or other permission denied
 *       event.
 *   <li><b>200 HTTP code</b> in front of a successful authentication or logout success action.
 * </ul>
 *
 * These simply returns are done because the default spring security way is to redirect, in case of
 * error to an error page and , in case of success login or logout, to a welcome page or to the
 * login-page address. This is not possible in a context where the client-side and the server-side
 * work independently. So in this way the server-side return to the client-side that the action was
 * ended with an error and the routing will be delegate to the client-side.
 *
 * @author Enrico Schintu
 */
public class SimpleResponseHandler implements ResponseHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(SimpleResponseHandler.class);

  private static final String UNAUTHORIZED_MESSAGE = "Authentication failure - {0} ";

  private static final String FORBIDDEN_MESSAGE = "Your session has timed out. Please login again.";

  @PostConstruct
  public void initialize() {}

  @Override
  public void onAuthenticationSuccess(
      HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
    LOGGER.info("Authentication successfully done.");
    LOGGER.debug("Authentication successfully done.");
    Map<String, Object> responseBody = new HashMap<>();
    responseBody.put(
        JwtUtils.TOKEN_HEADER_KEY,
        JwtUtils.createToken(
            Objects.requireNonNull(
                authentication, "Please, provide with a valid authentication.")));
    WebUtilities.addResponseInformation(
        Objects.requireNonNull(response, "Please, provide with a valid HTTP response."),
        responseBody,
        HttpServletResponse.SC_OK);
  }

  @Override
  public void onLogoutSuccess(
      HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
    response.setStatus(HttpServletResponse.SC_OK);
  }

  @Override
  public void commence(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final AuthenticationException exception) {
    String errorMessage = MessageFormat.format(UNAUTHORIZED_MESSAGE, exception.getMessage());
    LOGGER.error(errorMessage);
    UnauthorizedException exceptionThrow = new UnauthorizedException(errorMessage);
    String responseBody = WebUtilities.createErrorResponseMessage(exceptionThrow);
    WebUtilities.addResponseInformation(response, responseBody, exceptionThrow.getHttpStatusCode());
  }

  @Override
  public void handle(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final AccessDeniedException exception) {
    LOGGER.error(exception.getMessage());
    ForbiddenAccessException exceptionThrow = new ForbiddenAccessException(FORBIDDEN_MESSAGE);
    String responseBody = WebUtilities.createErrorResponseMessage(exceptionThrow);
    WebUtilities.addResponseInformation(response, responseBody, exceptionThrow.getHttpStatusCode());
  }

  @Override
  public void onAuthenticationFailure(
      HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
    String errorMessage =
        MessageFormat.format("Authentication failure - {0} ", exception.getMessage());
    LOGGER.error(errorMessage);

    UnauthorizedException exceptionThrow = new UnauthorizedException(errorMessage);
    String responseBody = WebUtilities.createErrorResponseMessage(exceptionThrow);
    WebUtilities.addResponseInformation(response, responseBody, exceptionThrow.getHttpStatusCode());
  }
}
