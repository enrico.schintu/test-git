package com.nivaura.esercizio.springsecurity.filters;

import com.nivaura.esercizio.springsecurity.exceptions.ForbiddenAccessException;
import com.nivaura.esercizio.springsecurity.exceptions.InvalidTokenException;
import com.nivaura.esercizio.springsecurity.exceptions.UnauthorizedException;
import com.nivaura.esercizio.springsecurity.utils.JwtUtils;
import com.nivaura.esercizio.springsecurity.utils.SecurityContextUtilities;
import com.nivaura.esercizio.springsecurity.utils.WebUtilities;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.MessageFormat;

/**
 * This filter extends {@link BasicAuthenticationFilter} and checks for the presence of the token in
 * the request in case the user is logged into the application.
 *
 * <li>If the user is not logged into the application then the filter is not applied</li>
 *
 * <li>If the user is logged into the application and provides a valid token then the request is
 * considered valid</li>
 *
 * <li>If the user is logged in but does not provide a valid token then the <b>403</b> error is
 * sent</li>
 *
 * <li>If an error occurs while reading the token then a <b>403</b> error is sent</li>
 *
 * @author Enrico Schintu
 */
public class AuthorizationTokenFilter extends BasicAuthenticationFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationTokenFilter.class);

    private RequestMatcher requiresAuthenticationRequestMatcher;

    public AuthorizationTokenFilter(AuthenticationManager authManager,
                                    String path) {
        super(authManager);
        LOGGER.info("Starting to load beans from {}...", AuthorizationTokenFilter.class.getName());
        requiresAuthenticationRequestMatcher = new AntPathRequestMatcher(path);
    }

    /**
     * This method verifies whether the user needs to provide the token to access resources
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(JwtUtils.TOKEN_HEADER_KEY);

        String username = SecurityContextUtilities.getUsername();

        if (!requiresAuthenticationRequestMatcher.matches(request)) {
            chain.doFilter(request, response);
            return;
        }
        // If the user isn't logged then this filter should be bypassed
        if (username != null) {
            try {
                // If the user is logged and the token isn't provided or isn't valid then an error 401 is sent.
                if (header == null || !validateToken(header, username)) {
                    String errorMessage = "Invalid token received.";
                    LOGGER.error(errorMessage);
                    UnauthorizedException exception = new UnauthorizedException(errorMessage);
                    String responseBody = WebUtilities.createErrorResponseMessage(exception);
                    WebUtilities.addResponseInformation(response, responseBody, exception.getHttpStatusCode());
                    return;
                }
            } catch (InvalidTokenException e) {
                // In case of error during the token validation, a 403 http error will be thrown
                String errorMessage = MessageFormat.format("Error during the validation token {0}", e.getMessage());
                LOGGER.error(errorMessage);
                ForbiddenAccessException exception = new ForbiddenAccessException(errorMessage);
                String responseBody = WebUtilities.createErrorResponseMessage(exception);
                WebUtilities.addResponseInformation(response, responseBody, exception.getHttpStatusCode());
                return;
            }
        }
        chain.doFilter(request, response);
    }

    /**
     * Verify that the user logged in the application matches the user contained in the token
     *
     * @param bearerToken
     * @param loggedUser
     * @return boolean
     * @throws InvalidTokenException
     */
    private boolean validateToken(String bearerToken,
                                  String loggedUser) throws InvalidTokenException {
        boolean result = false;
        if (StringUtils.isNotBlank(bearerToken) && StringUtils.startsWith(bearerToken, JwtUtils.TOKEN_PREFIX)) {
            result = StringUtils.equals(JwtUtils.resolveToken(bearerToken), loggedUser);
        }
        return result;
    }
}
