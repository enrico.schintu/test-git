package com.nivaura.esercizio.springsecurity.exceptions;

import org.apache.http.HttpStatus;

/**
 * This exception will be thrown when the request was valid, but the server is refusing action. The
 * user might not have the necessary permissions for a resource, or may need an account of some
 * sort.
 *
 * <p>By default its HTTP error code is <b>403</b> (Forbidden). It is possible configure the message
 * exception during initialization.
 *
 * @author Enrico Schintu
 */
public class ForbiddenAccessException extends HttpException {

  private static final long serialVersionUID = -5421800675849559215L;

  /** @param message */
  public ForbiddenAccessException(String message) {
    super(message, HttpStatus.SC_FORBIDDEN);
  }
}
