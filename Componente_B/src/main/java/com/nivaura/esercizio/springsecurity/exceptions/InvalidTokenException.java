package com.nivaura.esercizio.springsecurity.exceptions;

/**
 * Custom exception thrown because the validation of a Token, for example in case of an
 * authentication token, was failed for some reason. It extends {@link Exception} because the alert
 * must be managed.
 *
 * @author Enrico Schintu
 */
public class InvalidTokenException extends Exception {

  private static final long serialVersionUID = 230128263520857966L;

  public InvalidTokenException(String message, Throwable exception) {
    super(message, exception);
  }
}
