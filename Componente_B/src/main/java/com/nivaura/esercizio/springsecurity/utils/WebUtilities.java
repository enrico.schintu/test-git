package com.nivaura.esercizio.springsecurity.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nivaura.esercizio.springsecurity.exceptions.HttpException;
import com.nivaura.esercizio.springsecurity.exceptions.InternalErrorException;
import com.nivaura.esercizio.springsecurity.utils.functions.JsonConverterFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * This utility class contains all common operation done during an elaboration with HTTP
 * communication, for example to manage the HTTP Response content.
 *
 * @author Enrico Schintu
 */
public abstract class WebUtilities {

  private static final Logger LOGGER = LoggerFactory.getLogger(WebUtilities.class);

  /**
   * Add further information to the {@link HttpServletResponse} in JSON format and return the
   * response populated.
   *
   * <p>This method could be throws an {@link InternalErrorException} in case of:
   *
   * <ul>
   *   <li>IOException during the write on the response
   *   <li>JsonProcessingException during the conversion of input content object
   * </ul>
   *
   * @param response to populate
   * @param content Object to convert in JSON format
   * @param httpCode the exit http status of response
   * @throws InternalErrorException in case of Exception
   */
  public static void addResponseInformation(
      HttpServletResponse response, Object content, int httpCode) {
    try {
      addResponseInformation(
          response, JsonConverterFunctions.convertObjectToJson(content), httpCode);
    } catch (JsonProcessingException e) {
      throw new InternalErrorException(
          MessageFormat.format("An error occurs during the response write: {0}", e.getMessage()));
    }
  }

  /**
   * Add further information to the {@link HttpServletResponse} and return the response populated.
   *
   * <p>This method could be throws an {@link InternalErrorException} in case of:
   *
   * <ul>
   *   <li>IOException during the write on the response
   * </ul>
   *
   * @param response to populate
   * @param content Object to convert in JSON format
   * @param httpCode the exit http status of response
   * @throws InternalErrorException in case of Exception
   */
  public static void addResponseInformation(
      HttpServletResponse response, String content, int httpCode) {
    response.setContentType("application/json");
    try (PrintWriter out = response.getWriter()) {
      out.print(content);
      response.setStatus(httpCode);
    } catch (IOException e) {
      throw new InternalErrorException(
          MessageFormat.format("An error occurs during the response write: {0}", e.getMessage()));
    }
  }

  /**
   * Create the JSON message to map the input exception. The typical JSON to return is:
   *
   * <pre>
   * {
   * "message": "Exception message",
   * "code": 500,
   * "type": "InternalErrorException"
   * }
   * </pre>
   *
   * This method format the exception object using {@link JsonConverterFunctions}. In case of error
   * the response will be manually create to not return any errors.
   *
   * @param exception
   * @return
   */
  public static <T extends HttpException> String createErrorResponseMessage(T exception) {
    String responseBody = "";
    // configure a map for the JSON formatter
    Map<String, Object> responseBodyTemplate = new HashMap<>();
    responseBodyTemplate.put("message", exception.getMessage());
    responseBodyTemplate.put("code", exception.getHttpStatusCode());
    responseBodyTemplate.put("type", exception.getHttpStatus());
    try {
      responseBody = JsonConverterFunctions.convertObjectToJson(responseBodyTemplate);
    } catch (JsonProcessingException jsonException) {
      LOGGER.warn("An error occurs during the JSON conversion.");
      responseBody =
          String.format(
              "{\"message\":\"%s\",\"code\":%d,\"type\":\"%s\"}",
              exception.getMessage(), exception.getHttpStatusCode(), exception.getHttpStatus());
    }
    return responseBody;
  }
}
