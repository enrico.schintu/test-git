package com.nivaura.esercizio.springsecurity.configurations;

import com.nivaura.esercizio.springsecurity.filters.AuthorizationTokenFilter;
import com.nivaura.esercizio.springsecurity.handlers.SimpleResponseHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String SECURITY_PATTERN = "/api/**";

    @Override
    public void configure(WebSecurity web) {
        // OPTIONS
        web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");

        // Base ignore
        web.ignoring()
                .antMatchers("/app/**/*.{js,html}")
                .antMatchers("/i18n/**")
                .antMatchers("/content/**")
                .antMatchers("/test/**")
                .antMatchers("/h2-console/**");
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        SimpleResponseHandler simpleResponseHandler = new SimpleResponseHandler();
        http.csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/admin/**")
                .hasRole("ADMIN")
                .antMatchers("/anonymous*")
                .anonymous()
                .antMatchers("/index*")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers(SECURITY_PATTERN)
                .authenticated()
                .and()
                .exceptionHandling()
                .accessDeniedHandler(simpleResponseHandler)
                .authenticationEntryPoint(simpleResponseHandler)
                .and()
                .formLogin()
                .loginPage("/authentication")
                .successHandler(simpleResponseHandler)
                .failureHandler(simpleResponseHandler).permitAll()
//                .loginProcessingUrl("/login")
//                .defaultSuccessUrl("/homepage.html", true)
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessHandler(simpleResponseHandler)
                .deleteCookies("JSESSIONID")
                .and()
                .addFilter(new AuthorizationTokenFilter(authenticationManager(), SECURITY_PATTERN));
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
