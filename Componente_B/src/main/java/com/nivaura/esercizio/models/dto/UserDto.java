package com.nivaura.esercizio.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDto {

  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  private Long id;

  private String username;

  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  private String password;
}
