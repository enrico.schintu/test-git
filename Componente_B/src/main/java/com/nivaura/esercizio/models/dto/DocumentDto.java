package com.nivaura.esercizio.models.dto;

import com.nivaura.esercizio.models.commons.AuditFields;

import java.util.HashMap;
import java.util.Map;

public class DocumentDto extends AuditFields {

    private Long id;

    private String title;

    private String body;

    private final Map<String, Object> fields = new HashMap<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Map<String, Object> getFields() {
        return fields;
    }
}
