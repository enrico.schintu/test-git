package com.nivaura.esercizio;

import com.nivaura.esercizio.springsecurity.utils.NewUtilityClass;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsercizioApplication {

	public static void main(String[] args) {
		NewUtilityClass.helloWorld();
		SpringApplication.run(EsercizioApplication.class, args);
	}

}
