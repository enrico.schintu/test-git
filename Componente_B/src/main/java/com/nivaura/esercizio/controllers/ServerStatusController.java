package com.nivaura.esercizio.controllers;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("test/status")
public class ServerStatusController {

    @GetMapping()
    public void checkStatus() {
        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<String> response =
                    restTemplate.exchange(
                            "http://localhost:8001", HttpMethod.GET,
                            null,
                            new ParameterizedTypeReference<String>() {
                            });
        } catch (Exception ex) {
            throw new IllegalStateException("Component C isn't available.");
        }
    }
}
