package com.nivaura.esercizio.controllers;

import com.nivaura.esercizio.models.beans.PasswordChangeInfo;
import com.nivaura.esercizio.models.dto.PasswordChangeDto;
import com.nivaura.esercizio.services.UserService;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("api/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/change-password")
    public void changeUserPassword(Principal principal, @NonNull PasswordChangeDto passwordChangeDto) {
        PasswordChangeInfo passwordChangeInfo = new PasswordChangeInfo();
        passwordChangeInfo.setUsername(principal.getName());
        passwordChangeInfo.setCurrentPassword(passwordChangeDto.getCurrentPassword());
        passwordChangeInfo.setNewPassword(passwordChangeDto.getNewPassword());
        this.userService.updatePassword(passwordChangeInfo);
    }
}
