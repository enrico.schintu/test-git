package com.nivaura.esercizio.controllers;

import com.nivaura.esercizio.models.beans.DocumentInfo;
import com.nivaura.esercizio.models.dto.DocumentDto;
import com.nivaura.esercizio.services.DocumentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/documents")
public class DocumentController {

    private final DocumentService documentService;

    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @GetMapping
    public ResponseEntity<List<DocumentDto>> getDocuments() {
        List<DocumentDto> result = this.documentService.getDocuments().stream().map(this::convertInDocumentDto).collect(Collectors.toList());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    private DocumentDto convertInDocumentDto(DocumentInfo info) {
        DocumentDto dto = new DocumentDto();
        dto.setId(info.getId());
        dto.setTitle(info.getTitle());
        dto.setBody(info.getBody());
        dto.getFields().putAll(info.getFields());
        dto.setCreatedBy(info.getCreatedBy());
        dto.setCreateAt(info.getCreateAt());
        dto.setUpdatedBy(info.getUpdatedBy());
        dto.setUpdatedAt(info.getUpdatedAt());
        return dto;
    }
}
