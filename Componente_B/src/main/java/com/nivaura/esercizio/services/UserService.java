package com.nivaura.esercizio.services;

import com.nivaura.esercizio.models.beans.PasswordChangeInfo;
import com.nivaura.esercizio.models.beans.UserInfo;

public interface UserService {

    UserInfo findUserByUsername(String username);

    UserInfo saveUser(UserInfo user);

    void updatePassword(PasswordChangeInfo passwordChangeInfo);
}
