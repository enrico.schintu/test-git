package com.nivaura.esercizio.services.jpa;

import com.nivaura.esercizio.extensions.jpa.UsersRepository;
import com.nivaura.esercizio.models.beans.PasswordChangeInfo;
import com.nivaura.esercizio.models.beans.UserInfo;
import com.nivaura.esercizio.models.dao.UserDao;
import com.nivaura.esercizio.services.UserService;
import com.nivaura.esercizio.springsecurity.utils.PasswordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Optional;

/**
 * JPA implementation of {@link UserService}. This service defines the logic that concern user management using JPA features.
 *
 * @author Enrico
 */
@Service
public class JpaUserService implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JpaUserService.class);

    private final UsersRepository usersRepository;

    public JpaUserService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public UserInfo findUserByUsername(@NonNull String username) {
        LOGGER.debug(MessageFormat.format("Searching the user with username {0}.", username));
        return Optional.of(username)
                .map(usersRepository::findByUsername)
                .map(this::createUserInfo)
                .orElse(null);
    }

    @Override
    public UserInfo saveUser(@NonNull UserInfo user) {
        LOGGER.debug(MessageFormat.format("Saving the user with username {0}.", user.getUsername()));
        return Optional.of(user)
                .map(this::createUserDao)
                .map(usersRepository::save)
                .map(this::createUserInfo)
                .orElseThrow(() -> new IllegalStateException("Something went wrong saving the user."));
    }

    @Override
    public void updatePassword(@NonNull PasswordChangeInfo passwordChangeInfo) {
        LOGGER.debug(MessageFormat.format("Updating password of the user with username {0}.", passwordChangeInfo.getUsername()));
        Optional<PasswordChangeInfo> passwordInfo = Optional.of(passwordChangeInfo);

        UserInfo target = passwordInfo.map(PasswordChangeInfo::getUsername)
                .map(this::findUserByUsername)
                .orElseThrow(() -> new IllegalStateException("The user doesn't exist."));

        boolean currentPasswordMatch = passwordInfo.map(PasswordChangeInfo::getCurrentPassword)
                .filter(password -> PasswordUtils.matches(password, target.getPassword()))
                .map(obj -> true)
                .orElseThrow(() -> new IllegalStateException("The current password isn't correct."));

        if (currentPasswordMatch) {
            String newPassword = passwordInfo.map(PasswordChangeInfo::getNewPassword)
                    .filter(PasswordUtils::isValidPassword)
                    .map(PasswordUtils::encodePassword)
                    .orElseThrow(() -> new IllegalStateException("The new password isn't valid. The new password must have minimum eight characters, at least one letter and one number."));

            UserDao dao = this.createUserDao(target);
            dao.setPassword(newPassword);
            dao.setUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
            dao.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            usersRepository.save(dao);
        }
    }

    private UserDao createUserDao(UserInfo user) {
        UserDao dao = new UserDao();
        dao.setId(user.getId());
        dao.setUsername(user.getUsername());
        dao.setPassword(user.getPassword());
        dao.setCreatedBy(user.getCreatedBy());
        dao.setCreateAt(user.getCreateAt());
        dao.setUpdatedBy(user.getUpdatedBy());
        dao.setUpdatedAt(user.getUpdatedAt());
        return dao;
    }

    private UserInfo createUserInfo(UserDao dao) {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(dao.getId());
        userInfo.setUsername(dao.getUsername());
        userInfo.setPassword(dao.getPassword());
        userInfo.setCreatedBy(dao.getCreatedBy());
        userInfo.setCreateAt(dao.getCreateAt());
        userInfo.setUpdatedBy(dao.getUpdatedBy());
        userInfo.setUpdatedAt(dao.getUpdatedAt());
        return userInfo;
    }
}
