package com.nivaura.esercizio.services;

import com.nivaura.esercizio.models.beans.DocumentInfo;

import java.util.List;

public interface DocumentService {

  List<DocumentInfo> getDocuments();
}
