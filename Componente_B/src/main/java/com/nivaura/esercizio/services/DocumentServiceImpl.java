package com.nivaura.esercizio.services;

import com.nivaura.esercizio.models.beans.DocumentInfo;
import com.nivaura.esercizio.springsecurity.utils.SecurityContextUtilities;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of {@link DocumentService}. This service defines the logic that concern document manipulation.
 *
 * @author Enrico
 */
@Service
public class DocumentServiceImpl implements DocumentService {

    /**
     * The Component C API URL.
     */
    private static final String COMPONENT_C_DATA_URL = "http://localhost:8001/data";

    /**
     * The document template file
     */
    @Value("classpath:templates/document_template.txt")
    private Resource templateFile;

    @PostConstruct
    private void initialize() {
        Objects.requireNonNull(templateFile, "Please provide a valid template file");
    }

    @Override
    public List<DocumentInfo> getDocuments() {
        String documentBody = new String(this.getDocumentTemplate());
        return this.retrieveDocumentInfo(documentBody);
    }

    /**
     * The list of documents. Each document fields info set returned from Component C is stored in a {@link DocumentInfo} object.
     */
    private List<DocumentInfo> retrieveDocumentInfo(String documentBody) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<List<Map<String, Object>>> response =
                    restTemplate.exchange(
                            COMPONENT_C_DATA_URL, HttpMethod.GET,
                            null,
                            new ParameterizedTypeReference<List<Map<String, Object>>>() {
                            });
            List<Map<String, Object>> fields = Optional.ofNullable(response.getBody()).orElseGet(ArrayList::new);
            return fields.stream().map(this::createDocumentWithFields).peek(doc -> doc.setBody(documentBody)).collect(Collectors.toList());
        } catch (Exception ex) {
            throw new IllegalStateException("Component C isn't available.");
        }
    }

    private byte[] getDocumentTemplate() {
        try {
            File file = templateFile.getFile();
            return Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private DocumentInfo createDocumentWithFields(Map<String, Object> fields) {
        DocumentInfo document = new DocumentInfo();
        document.getFields().putAll(fields);
        document.setCreatedBy(SecurityContextUtilities.getUsername());
        document.setCreateAt(new Timestamp(System.currentTimeMillis()));
        document.setUpdatedBy(SecurityContextUtilities.getUsername());
        document.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
        return document;
    }
}
