package com.nivaura.esercizio.services.jpa;

import com.nivaura.esercizio.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Service that manages the authentication of the user with credentials provided in the login form.
 *
 * @author Enrico Schintu
 */
@Service
public class UserAuthenticationService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserAuthenticationService.class);

    private final UserService userService;

    public UserAuthenticationService(UserService userService) {
        LOGGER.info("Starting to load beans from {}...", UserAuthenticationService.class.getName());
        this.userService = userService;
    }

    /**
     * Return an UserDetails object populated with the user's data extracted from the database.
     *
     * @param username the username of the user who perform login
     */
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.info(MessageFormat.format("User with username `{0}` wants to login.", username));
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        return Optional.ofNullable(username)
                .map(userService::findUserByUsername)
                .map(user -> new User(user.getUsername(), user.getPassword(), grantedAuthorities))
                .orElseThrow(() -> new UsernameNotFoundException("Username Not found."));
    }
}
