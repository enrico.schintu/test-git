package com.nivaura.esercizio.springsecurity.utils;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

/**
 * Test class of {@link PasswordUtils}
 *
 * @author Enrico Schintu
 */
public class PasswordUtilsTest {

    private static final String PASSWORD_TEST = "myTestPassword";

    @Test
    @DisplayName("[encodePassword] Should encode the provided password.")
    public void encodePassword() {
        String result = PasswordUtils.encodePassword(PASSWORD_TEST);
        Assertions.assertTrue(StringUtils.isNotBlank(result));
        Assertions.assertNotEquals(PASSWORD_TEST, result);
    }

    @Test
    @DisplayName("[matches] The encoded password should match with the raw password.")
    public void matches() {
        String encoded = PasswordUtils.encodePassword(PASSWORD_TEST);
        Assertions.assertTrue(PasswordUtils.matches(PASSWORD_TEST, encoded));
    }

    @Test
    @DisplayName("[isValidPassword] Should check if the provided password is a good password.")
    public void isValidPassword() {
        String badPassword = "Test";
        String goodPassword = "T3stStr0ng";
        Assertions.assertFalse(PasswordUtils.isValidPassword(badPassword));
        Assertions.assertTrue(PasswordUtils.isValidPassword(goodPassword));
    }

}
