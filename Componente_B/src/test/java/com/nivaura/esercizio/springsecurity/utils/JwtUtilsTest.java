package com.nivaura.esercizio.springsecurity.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nivaura.esercizio.springsecurity.exceptions.InvalidTokenException;
import com.nivaura.esercizio.springsecurity.utils.functions.JsonConverterFunctions;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Test class of {@link JwtUtils}
 *
 * @author Enrico Schintu
 */
public class JwtUtilsTest {

    private static final String VALID_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ0ZXN0IiwiaWF0IjoxNTcwODY0NTg5LCJleHAiOjIyMzM1NTI1ODksImF1ZCI6Ind3dy50ZXN0Lml0Iiwic3ViIjoidGVzdEB0ZXN0Lml0IiwiR2l2ZW5OYW1lIjoidGVzdCIsIlN1cm5hbWUiOiJ0ZXN0IiwiRW1haWwiOiJ0ZXN0QHRlc3QuaXQiLCJSb2xlIjpbIkFkbWluIiwiUHJvamVjdCBBZG1pbmlzdHJhdG9yIl19.U8oqvJ-e9k35Dr5ZQ71xdzBQobRHnudW0OUuEhOHPsM";

    private static final String VALID_TOKEN_HEADER = "Bearer eyJhbGciOiJIUzUxMiJ9";

    private static final String TEST_USER = "test@test.it";

    private Authentication auth;

    @BeforeEach
    public void setup() {
        auth = new UsernamePasswordAuthenticationToken(TEST_USER, "test_user", AuthorityUtils.createAuthorityList("ROLE_1"));
    }

    @Test
    @DisplayName("[creteToken] Should create the token using the informations contained in the {@link Authentication} privided.")
    public void creteTokenTest() {
        String token = JwtUtils.createToken(auth);

        String[] tokenComponents = token.split("\\.");
        String header = tokenComponents[0];
        String payload = tokenComponents[1];
        String signingKey = tokenComponents[2];

        Assertions.assertTrue(StringUtils.isNotBlank(token));
        Assertions.assertEquals(VALID_TOKEN_HEADER, header);
        Assertions.assertTrue(StringUtils.isNotBlank(payload));
        Assertions.assertTrue(StringUtils.isNotBlank(signingKey));
    }

    @Test
    @DisplayName("[createJsonToken] It must create a string containing the token in json format.")
    public void createJsonTokenTest() throws JsonProcessingException {
        String jsonToken = this.createJsonToken(auth);
        Assertions.assertTrue(StringUtils.isNotBlank(jsonToken));
        Assertions.assertTrue(StringUtils.contains(jsonToken, JwtUtils.TOKEN_HEADER_KEY));
        Assertions.assertTrue(StringUtils.contains(jsonToken, JwtUtils.TOKEN_PREFIX));
        Assertions.assertTrue(StringUtils.contains(jsonToken, VALID_TOKEN_HEADER));
    }

    @Test
    @DisplayName("[resolveToken] It must return the subject contained in the token provided.")
    public void resolveTokenTest() throws InvalidTokenException {
        String subject = JwtUtils.resolveToken(VALID_TOKEN);

        Assertions.assertTrue(StringUtils.isNotBlank(subject));
        Assertions.assertEquals(TEST_USER, subject);
    }

    @Test
    @DisplayName("[invalidToken] It must throw an InvalidTokenException because th token is not valid.")
    public void resolveTokenFailTest() {
        Assertions.assertThrows(InvalidTokenException.class, () -> JwtUtils.resolveToken("INVALID TOKEN"));
    }


    /**
     * Create a JWT wrapped with the same format of an HTTP Request Header.
     *
     * @param authentication
     * @return
     * @throws JsonProcessingException
     */
    private String createJsonToken(Authentication authentication) throws JsonProcessingException {
        Map<String, String> jsonTokenTemplate = new HashMap<>();
        jsonTokenTemplate.put(JwtUtils.TOKEN_HEADER_KEY, JwtUtils.createToken(authentication));
        return JsonConverterFunctions.convertObjectToJson(jsonTokenTemplate);
    }

}
